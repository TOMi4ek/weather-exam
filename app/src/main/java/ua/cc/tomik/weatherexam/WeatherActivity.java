package ua.cc.tomik.weatherexam;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import ua.cc.tomik.weatherexam.Api.ApiService;
import ua.cc.tomik.weatherexam.Model.AllWeatherData;
import ua.cc.tomik.weatherexam.Model.List;

public class WeatherActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        if (getIntent().getExtras() != null) {
            double latitude = getIntent().getExtras().getDouble(MainActivity.LATITUDE_INTENT);
            double longitude = getIntent().getExtras().getDouble(MainActivity.LONGITUDE_INTENT);

            ApiService apiService = RetrofitClient.getApiService();

            Call<AllWeatherData> call = apiService.getWeatherJSON(latitude, longitude, getString(R.string.weather_api_key));

            call.enqueue(new Callback<AllWeatherData>() {
                @Override
                public void onResponse(@NonNull Call<AllWeatherData> call, @NonNull Response<AllWeatherData> response) {
                    AllWeatherData allWeatherData = response.body();
                    if (response.isSuccessful() && allWeatherData != null) {

                        WeatherAdapter weatherAdapter = new WeatherAdapter(allWeatherData.getList());

                        RecyclerView recyclerView = findViewById(R.id.recycler_view);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        recyclerView.setAdapter(weatherAdapter);
                        setDataForGraph(allWeatherData.getList());
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.get_weather_fail, Toast.LENGTH_SHORT)
                                .show();
                        WeatherActivity.super.onBackPressed();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AllWeatherData> call, @NonNull Throwable t) {
                    Toast.makeText(getApplicationContext(), R.string.get_weather_fail, Toast.LENGTH_SHORT)
                            .show();
                    WeatherActivity.super.onBackPressed();

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), R.string.intent_error, Toast.LENGTH_SHORT)
                    .show();
            onBackPressed();
        }


    }

    private void setDataForGraph(ArrayList<List> list) {
        GraphView graph = findViewById(R.id.graph);
        DataPoint[] dataPointArray = new DataPoint[list.size()];
        for (int i = 0; i < list.size(); i++) {
            dataPointArray[i] = new DataPoint(i, list.get(i).getMain().getTemp());
        }
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPointArray);



        graph.getViewport().setMinX(0.0);
        graph.getViewport().setMaxX(list.size());
        graph.addSeries(series);

        String[] labels = getResources().getStringArray(R.array.graph_grid_labels);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph,labels, null);

        graph.getGridLabelRenderer().setHorizontalAxisTitle(getString(R.string.graph_label));
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
    }


}