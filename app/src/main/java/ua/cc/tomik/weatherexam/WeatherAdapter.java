package ua.cc.tomik.weatherexam;


import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ua.cc.tomik.weatherexam.Model.List;
import ua.cc.tomik.weatherexam.Model.Main;
import ua.cc.tomik.weatherexam.Model.Weather;

public class WeatherAdapter extends RecyclerView.Adapter<CustomViewHolder> {
    private ArrayList<List> weatherData;
    private static final String IMAGE_BASE_URL = "http://openweathermap.org/img/w/";
    private static final String IMAGE_FORMAT = ".png";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

    WeatherAdapter(ArrayList<List> weatherData) {
        this.weatherData = weatherData;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_list_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        Context context = holder.itemView.getContext();

        List oneDayWeatherData = weatherData.get(position);
        Weather weather = oneDayWeatherData.getWeather().get(0);
        Main main = oneDayWeatherData.getMain();

        Date date = new Date(oneDayWeatherData.getDt() * 1000);

        holder.getWeatherDate().setText(simpleDateFormat.format(date));

        holder.getWeatherTemperature().setText(context.getString(R.string.Temperature, main.getTemp()));

        holder.getWeatherHumidity().setText(context.getString(R.string.Humidity, main.getHumidity()));

        holder.getWeatherPrecipitation().setText(weather.getDescription());

        String imageURL = IMAGE_BASE_URL + weather.getIcon() + IMAGE_FORMAT;

        Picasso.with(context)
                .load(imageURL)
                .resizeDimen(R.dimen.icon_size,R.dimen.icon_size)
                .into(holder.getWeatherIcon());
    }

    @Override
    public int getItemCount() {
        return weatherData != null ? weatherData.size() : 0;
    }
}

