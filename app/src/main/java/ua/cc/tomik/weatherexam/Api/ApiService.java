package ua.cc.tomik.weatherexam.Api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ua.cc.tomik.weatherexam.Model.AllWeatherData;

public interface ApiService {

    @GET("forecast?units=metric")
    Call<AllWeatherData> getWeatherJSON(@Query("lat") double latitude,
                                        @Query("lon") double longitude,
                                        @Query("appid") String apiKey);


}
