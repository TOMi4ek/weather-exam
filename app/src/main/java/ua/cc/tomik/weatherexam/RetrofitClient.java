package ua.cc.tomik.weatherexam;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import ua.cc.tomik.weatherexam.Api.ApiService;

class RetrofitClient {
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }
}
