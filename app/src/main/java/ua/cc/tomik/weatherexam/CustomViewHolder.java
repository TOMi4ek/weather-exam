package ua.cc.tomik.weatherexam;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

class CustomViewHolder extends RecyclerView.ViewHolder {
    private TextView weatherDate, weatherTemperature, weatherHumidity, weatherPrecipitation;
    private ImageView weatherIcon;

    CustomViewHolder(View itemView) {
        super(itemView);
        weatherDate = itemView.findViewById(R.id.weatherDate);
        weatherTemperature = itemView.findViewById(R.id.weatherTemperature);
        weatherHumidity = itemView.findViewById(R.id.weatherHumidity);
        weatherPrecipitation = itemView.findViewById(R.id.weatherPrecipitation);
        weatherIcon = itemView.findViewById(R.id.weatherIcon);
    }

    TextView getWeatherDate() {
        return weatherDate;
    }

    public void setWeatherDate(TextView weatherDate) {
        this.weatherDate = weatherDate;
    }

    TextView getWeatherTemperature() {
        return weatherTemperature;
    }

    public void setWeatherTemperature(TextView weatherTemperature) {
        this.weatherTemperature = weatherTemperature;
    }


    TextView getWeatherHumidity() {
        return weatherHumidity;
    }

    public void setWeatherHumidity(TextView weatherHumidity) {
        this.weatherHumidity = weatherHumidity;
    }

    TextView getWeatherPrecipitation() {
        return weatherPrecipitation;
    }

    public void setWeatherPrecipitation(TextView weatherPrecipitation) {
        this.weatherPrecipitation = weatherPrecipitation;
    }

    ImageView getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(ImageView weatherIcon) {
        this.weatherIcon = weatherIcon;
    }
}