package ua.cc.tomik.weatherexam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class MainActivity extends AppCompatActivity {
    public static final String LATITUDE_INTENT = "latitude";
    public static final String LONGITUDE_INTENT = "longitude";
    public static final int PLACE_PICKER_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();

        autocompleteFragment.setFilter(autocompleteFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {

            @Override
            public void onPlaceSelected(Place place) {
                Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
                intent.putExtra(LATITUDE_INTENT, place.getLatLng().latitude);
                intent.putExtra(LONGITUDE_INTENT, place.getLatLng().longitude);
                startActivity(intent);
            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getApplicationContext(), R.string.city_select_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClick(View view) throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this,data);
                Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
                intent.putExtra(LATITUDE_INTENT, place.getLatLng().latitude);
                intent.putExtra(LONGITUDE_INTENT, place.getLatLng().longitude);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(), R.string.city_select_error, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
