
package ua.cc.tomik.weatherexam.Model;

import com.google.gson.annotations.SerializedName;

public class Precipitation {

    @SerializedName("3h")
   private Double level3H;

    public Double get3h() {
        return level3H;
    }

    public void set3h(Double level3H) {
        this.level3H = level3H;
    }

    @Override
    public String toString() {
        return "Precipitation{" +
                "level3H=" + level3H +
                '}';
    }
}
